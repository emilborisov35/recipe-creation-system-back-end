# Recipe Creation System Back-End

## Description

This is Team 13's back-end for the final project which Fourth gave to part of Telerik Academy Alpha's JavaScript cohort. Our aim is to design and implement a single-page web app which would allow restaurant chefs to create and manage recipes. Recipes will be composed of products with a known nutritional value. During recipe composition chefs should see changes in its total nutritional value of in real time - each time an ingredient is added, removed, or its quantity - modified. Newly created recipes could be tagged with a category from a pre-defined list.

### Tasks

- x Authentication - Log in
- x Read a list of previously created recipes
- x CRUD individual recipes which contain products and/or other recipes

### Stack

- Database - MariaDB
- REST API - NestJS

---

## Getting started

### Installation

Clone the repository

    git clone https://gitlab.com/emilborisov35/recipe-creation-system-back-end.git

Enter its folder

    cd recipe-creation-system-back-end/

Install project dependencies

    npm install

Create a `.env` file in the project's root directory with the following contents

    PORT=3000
    JWT_SECRET=VerySecr3t!
    DB_TYPE=mysql
    DB_HOST=localhost
    DB_PORT=3306
    DB_USERNAME=root
    DB_PASSWORD=root
    DB_DATABASE_NAME=recipes_db
    REDIS_HOST=127.0.0.1
    REDIS_PORT=6379
    REDIS_PASSWORD=root

Create a new DB schema in your RDBMS with the data from the `.env` file - set the schema name to `recipes_db`, admin username and password to `root`, etc. Alternatively, you could modify the settings of the `.env` file to match those of your existing DB connection.

Run the `TypeORM` migrations which will set up the necessary tables and relations

    npm run typeorm -- migration:run

Seed the project's database with initial data:

    npm run seed

Run the following command to start the project

    npm run start:dev

To make sure that everything worked correctly, you can send a log in request through `Postman` by sending a `POST` request to `http://localhost:3000/session` with the following body:

    {
      "username": "TestUser",
      "password": "Test1234%"
    }

The expected response is a JWT token. Here's an example of what that response would look like:

    {
      "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlRlc3RVc2VyIiwiaWF0IjoxNTY0NTY4NzI4LCJleHAiOjE1NjUxNzM1Mjh9.HO1tp36uFBn59b2mhZDHFd9cbUphLy64PtdiwnYigjs"
    }

---

### Database

#### MariaDB

The project uses [TypeORM](http://typeorm.io/) with [MariaDB](https://mariadb.org/). Create a new MariaDB schema with the name `recipes_db` and seed it by running `npm run seed`. You can find additional DB settings in the ormconfig.json. You also need a `.env` file in the project's root directory which we will provide upon request.

---

### npm scripts

- `npm start` - start the app
- `npm test` - run Jest unit tests
- `npm run start:dev` - start the app with nodemon
- `npm run typeorm -- <command>` - run TypeORM commands
- `npm run seed` - seed the database with the provided source data
- `npm run compodoc` - run Compodoc to see full documentation of the app
