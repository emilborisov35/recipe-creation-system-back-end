import 'reflect-metadata';
import { createConnection } from 'typeorm';
import * as bcrypt from 'bcrypt';
import * as fg from '../USDA_db/fd_group.json';
import * as fd from '../USDA_db/food_des.json';
// Warning! Do not open! Very large file - tends to freeze systems!
import * as nd from '../USDA_db/nut_data.json';
import * as ndef from '../USDA_db/nutr_def.json';
import * as w from '../USDA_db/weight.json';

import { User } from '../entities/user.entity';
import { Measure } from '../entities/measure.entity';
import { Nutrition } from '../entities/nutrition.entity';
import { Product } from '../entities/product.entity';

// Custom async forEach
async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

const main = async () => {
  const connection = await createConnection();

  const userRepository = connection.manager.getRepository(User);
  const productRepository = connection.manager.getRepository(Product);
  const measureRepository = connection.manager.getRepository(Measure);
  const nutritionRepository = connection.manager.getRepository(Nutrition);

  const foodGroup = fg;
  const foodDescription = fd;
  const nutrientData = nd as any;
  const nutrientDefinition = ndef;
  const weight = w;

  const populateDatabase = async () => {
    await asyncForEach(foodDescription, async (p) => {
      const product = await productRepository.create();
      product.code = p.NDB_No;
      product.description = p.Long_Desc;
      const productFoodGroup = foodGroup.find((g) => g.FdGrp_Cd === p.FdGrp_Cd);
      product.foodGroup = productFoodGroup.FdGrp_desc;
      await productRepository.save(product);

      const measures = weight.filter((wght) => wght.NDB_No === p.NDB_No);
      await asyncForEach(measures, async (m) => {
        const measure = new Measure();
        measure.measure = m.Msre_Desc;
        let amount = m.Amount;
        if (m.Msre_Desc.includes('package') && m.Amount !== '1') {
          amount = 1;
        }
        measure.gramsPerMeasure = amount * m.Gm_Wgt;
        measure.product = Promise.resolve(product);
        await measureRepository.save(measure);
      });

      const nutrition = new Nutrition();
      nutrientDefinition.forEach((n) => {
        const code = n.Nutr_no;
        const nutriData = nutrientData.find((nutr) => (+nutr.Nutr_No === code && +nutr.NDB_No === p.NDB_No));
        let value;
        if (nutriData === undefined) {
          value = 0;
        } else {
          value = nutriData.Nutr_Val;
        }

        nutrition[n.Tagname] = {
          description: n.NutrDesc,
          unit: n.Units,
          value,
        };
      });
      nutrition.product = Promise.resolve(product);
      nutrition.recipe = Promise.resolve(null);
      await nutritionRepository.save(nutrition);
    });

    const dobrin = await userRepository.findOne({
      where: {
        firstName: 'Dobrin',
      },
    });

    if (!dobrin) {
      const user = new User();
      user.username = 'Dobrin';
      const passwordHash = await bcrypt.hash('D0brin!', 10);
      user.password = passwordHash;
      user.email = 'dobrin@abv.bg';
      user.firstName = 'Dobrin';
      user.lastName = 'Yordanov';
      await userRepository.save(user);
    } else {
      console.log(`Dobrin is already in the db`);
    }

    const emo = await userRepository.findOne({
      where: {
        firstName: 'Emil',
      },
    });

    if (!emo) {
      const user = new User();
      user.username = 'emil';
      const passwordHash = await bcrypt.hash('Em1l#', 10);
      user.password = passwordHash;
      user.email = 'emil@emil.com';
      user.firstName = 'Emil';
      user.lastName = 'Borisov';
      await userRepository.save(user);
    } else {
      console.log(`Emil is already in the db`);
    }

    const testUser = await userRepository.findOne({
      where: {
        firstName: 'Test',
      },
    });

    if (!testUser) {
      const user = new User();
      user.username = 'TestUser';
      const passwordHash = await bcrypt.hash('Test1234%', 10);
      user.password = passwordHash;
      user.email = 'test@test.test';
      user.firstName = 'Test';
      user.lastName = 'User';
      await userRepository.save(user);
    } else {
      console.log(`TestUser is already in the db`);
    }

    connection.close();
  };

  populateDatabase();
};

main().catch(console.error);
