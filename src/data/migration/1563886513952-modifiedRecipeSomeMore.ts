import {MigrationInterface, QueryRunner} from "typeorm";

export class modifiedRecipeSomeMore1563886513952 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP INDEX `IDX_642147e73deda7e94f8fa87c5b` ON `recipes`");
        await queryRunner.query("CREATE TABLE `recipes_subrecipes_recipes` (`recipesId_1` varchar(36) NOT NULL, `recipesId_2` varchar(36) NOT NULL, INDEX `IDX_4961828cea86ab7c03b3f2d57b` (`recipesId_1`), INDEX `IDX_e35dfe3f2e86f5ee48437e52ee` (`recipesId_2`), PRIMARY KEY (`recipesId_1`, `recipesId_2`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `recipes_parent_recipes_recipes` (`recipesId_1` varchar(36) NOT NULL, `recipesId_2` varchar(36) NOT NULL, INDEX `IDX_9e81e1b8b7e51461732173d2c1` (`recipesId_1`), INDEX `IDX_a0d141f6f046cf61cab3a48b79` (`recipesId_2`), PRIMARY KEY (`recipesId_1`, `recipesId_2`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `users` CHANGE `joined` `joined` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `recipes` DROP FOREIGN KEY `FK_afd4f74f8df44df574253a7f37b`");
        await queryRunner.query("ALTER TABLE `recipes` DROP FOREIGN KEY `FK_642147e73deda7e94f8fa87c5bf`");
        await queryRunner.query("ALTER TABLE `recipes` DROP FOREIGN KEY `FK_5ceed9637ca602f6daa6c7f64b2`");
        await queryRunner.query("ALTER TABLE `recipes` CHANGE `authorId` `authorId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `recipes` CHANGE `nutritionId` `nutritionId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `recipes` CHANGE `recipeCategoryId` `recipeCategoryId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `recipe_products` DROP FOREIGN KEY `FK_b5d5a9de7331142dee95cd684fb`");
        await queryRunner.query("ALTER TABLE `recipe_products` DROP FOREIGN KEY `FK_e30971c79c4c37daf591e443c4f`");
        await queryRunner.query("ALTER TABLE `recipe_products` CHANGE `productCode` `productCode` int NULL");
        await queryRunner.query("ALTER TABLE `recipe_products` CHANGE `recipesId` `recipesId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `products` DROP FOREIGN KEY `FK_029502bbd9a8edca9ebb9ae652d`");
        await queryRunner.query("ALTER TABLE `products` CHANGE `nutritionId` `nutritionId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `measures` DROP FOREIGN KEY `FK_db5edcd1328fb776774cc41420e`");
        await queryRunner.query("ALTER TABLE `measures` CHANGE `productCode` `productCode` int NULL");
        await queryRunner.query("ALTER TABLE `recipes` ADD CONSTRAINT `FK_afd4f74f8df44df574253a7f37b` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipes` ADD CONSTRAINT `FK_642147e73deda7e94f8fa87c5bf` FOREIGN KEY (`nutritionId`) REFERENCES `nutritions`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipes` ADD CONSTRAINT `FK_5ceed9637ca602f6daa6c7f64b2` FOREIGN KEY (`recipeCategoryId`) REFERENCES `recipe_category`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipe_products` ADD CONSTRAINT `FK_b5d5a9de7331142dee95cd684fb` FOREIGN KEY (`productCode`) REFERENCES `products`(`code`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipe_products` ADD CONSTRAINT `FK_e30971c79c4c37daf591e443c4f` FOREIGN KEY (`recipesId`) REFERENCES `recipes`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `products` ADD CONSTRAINT `FK_029502bbd9a8edca9ebb9ae652d` FOREIGN KEY (`nutritionId`) REFERENCES `nutritions`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `measures` ADD CONSTRAINT `FK_db5edcd1328fb776774cc41420e` FOREIGN KEY (`productCode`) REFERENCES `products`(`code`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipes_subrecipes_recipes` ADD CONSTRAINT `FK_4961828cea86ab7c03b3f2d57b3` FOREIGN KEY (`recipesId_1`) REFERENCES `recipes`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipes_subrecipes_recipes` ADD CONSTRAINT `FK_e35dfe3f2e86f5ee48437e52ee7` FOREIGN KEY (`recipesId_2`) REFERENCES `recipes`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipes_parent_recipes_recipes` ADD CONSTRAINT `FK_9e81e1b8b7e51461732173d2c11` FOREIGN KEY (`recipesId_1`) REFERENCES `recipes`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipes_parent_recipes_recipes` ADD CONSTRAINT `FK_a0d141f6f046cf61cab3a48b79e` FOREIGN KEY (`recipesId_2`) REFERENCES `recipes`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `recipes_parent_recipes_recipes` DROP FOREIGN KEY `FK_a0d141f6f046cf61cab3a48b79e`");
        await queryRunner.query("ALTER TABLE `recipes_parent_recipes_recipes` DROP FOREIGN KEY `FK_9e81e1b8b7e51461732173d2c11`");
        await queryRunner.query("ALTER TABLE `recipes_subrecipes_recipes` DROP FOREIGN KEY `FK_e35dfe3f2e86f5ee48437e52ee7`");
        await queryRunner.query("ALTER TABLE `recipes_subrecipes_recipes` DROP FOREIGN KEY `FK_4961828cea86ab7c03b3f2d57b3`");
        await queryRunner.query("ALTER TABLE `measures` DROP FOREIGN KEY `FK_db5edcd1328fb776774cc41420e`");
        await queryRunner.query("ALTER TABLE `products` DROP FOREIGN KEY `FK_029502bbd9a8edca9ebb9ae652d`");
        await queryRunner.query("ALTER TABLE `recipe_products` DROP FOREIGN KEY `FK_e30971c79c4c37daf591e443c4f`");
        await queryRunner.query("ALTER TABLE `recipe_products` DROP FOREIGN KEY `FK_b5d5a9de7331142dee95cd684fb`");
        await queryRunner.query("ALTER TABLE `recipes` DROP FOREIGN KEY `FK_5ceed9637ca602f6daa6c7f64b2`");
        await queryRunner.query("ALTER TABLE `recipes` DROP FOREIGN KEY `FK_642147e73deda7e94f8fa87c5bf`");
        await queryRunner.query("ALTER TABLE `recipes` DROP FOREIGN KEY `FK_afd4f74f8df44df574253a7f37b`");
        await queryRunner.query("ALTER TABLE `measures` CHANGE `productCode` `productCode` int NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `measures` ADD CONSTRAINT `FK_db5edcd1328fb776774cc41420e` FOREIGN KEY (`productCode`) REFERENCES `products`(`code`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `products` CHANGE `nutritionId` `nutritionId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `products` ADD CONSTRAINT `FK_029502bbd9a8edca9ebb9ae652d` FOREIGN KEY (`nutritionId`) REFERENCES `nutritions`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipe_products` CHANGE `recipesId` `recipesId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `recipe_products` CHANGE `productCode` `productCode` int NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `recipe_products` ADD CONSTRAINT `FK_e30971c79c4c37daf591e443c4f` FOREIGN KEY (`recipesId`) REFERENCES `recipes`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipe_products` ADD CONSTRAINT `FK_b5d5a9de7331142dee95cd684fb` FOREIGN KEY (`productCode`) REFERENCES `products`(`code`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipes` CHANGE `recipeCategoryId` `recipeCategoryId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `recipes` CHANGE `nutritionId` `nutritionId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `recipes` CHANGE `authorId` `authorId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `recipes` ADD CONSTRAINT `FK_5ceed9637ca602f6daa6c7f64b2` FOREIGN KEY (`recipeCategoryId`) REFERENCES `recipe_category`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipes` ADD CONSTRAINT `FK_642147e73deda7e94f8fa87c5bf` FOREIGN KEY (`nutritionId`) REFERENCES `nutritions`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `recipes` ADD CONSTRAINT `FK_afd4f74f8df44df574253a7f37b` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users` CHANGE `joined` `joined` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("DROP INDEX `IDX_a0d141f6f046cf61cab3a48b79` ON `recipes_parent_recipes_recipes`");
        await queryRunner.query("DROP INDEX `IDX_9e81e1b8b7e51461732173d2c1` ON `recipes_parent_recipes_recipes`");
        await queryRunner.query("DROP TABLE `recipes_parent_recipes_recipes`");
        await queryRunner.query("DROP INDEX `IDX_e35dfe3f2e86f5ee48437e52ee` ON `recipes_subrecipes_recipes`");
        await queryRunner.query("DROP INDEX `IDX_4961828cea86ab7c03b3f2d57b` ON `recipes_subrecipes_recipes`");
        await queryRunner.query("DROP TABLE `recipes_subrecipes_recipes`");
        await queryRunner.query("CREATE UNIQUE INDEX `IDX_642147e73deda7e94f8fa87c5b` ON `recipes` (`nutritionId`)");
    }

}
