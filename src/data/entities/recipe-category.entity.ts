import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

import { Recipe } from './recipe.entity';

@Entity()
export class RecipeCategory {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  category: string;

  @OneToMany(type => Recipe, recipe => recipe.recipeCategory)
  recipes: Promise<Recipe[]>;
}
