import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, OneToOne, OneToMany, JoinColumn, JoinTable } from 'typeorm';

import { RecipeProduct } from './recipe-product';
import { User } from './user.entity';
import { Nutrition } from './nutrition.entity';
import { RecipeCategory } from './recipe-category.entity';

/**
 * Recipe entity
 */
@Entity('recipes')
export class Recipe {
  /**
   * Id of the recipe
   */
  @PrimaryGeneratedColumn('uuid')
  id: string;
  /**
   * Title
   */
  @Column('nvarchar')
  title: string;
  /**
   * Food group to which the product belongs
   */
  @Column('nvarchar')
  foodGroup: string;
  /**
   * Author of the recipe
   */
  @ManyToOne(type => User, user => user.recipes)
  author: Promise<User>;
  /**
   * Products in the recipe
   */
  @OneToMany(type => RecipeProduct, recipeProduct => recipeProduct.recipes, { eager: true })
  recipeProducts: RecipeProduct[];
  /**
   * Subrecipes in the recipe
   */
  @ManyToMany(type => Recipe, recipe => recipe.subrecipes)
  @JoinTable()
  subrecipes: Promise<Recipe[]>;

  @ManyToMany(type => Recipe, recipe => recipe.childRecipes)
  @JoinTable()
  parentRecipes: Recipe[];

  @ManyToMany(type => Recipe, recipe => recipe.parentRecipes)
  childRecipes: Recipe[];
  /**
   * Nutrient data for the recipe
   */
  @OneToOne(type => Nutrition, nutrition => nutrition.recipe, { eager: true })
  @JoinColumn()
  nutrition: Nutrition;
  /**
   * Is the recipe deleted
   */
  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;

  @ManyToOne(type => RecipeCategory, recipeCategory => recipeCategory.recipes)
  recipeCategory: Promise<RecipeCategory>;
}
