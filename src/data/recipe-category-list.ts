export class RecipeCategoryList {
  public static list: string[] = [
    'Soup',
    'Appetizer',
    'Salads',
    'Bread',
    'Drink',
    'Dessert',
    'Main Dish',
    'Sauce',
    'Side Dish',
  ];
}
