import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';

import { ShowRecipeDTO } from '../models/show-recipe.dto';
import { Recipe } from '../data/entities/recipe.entity';
import { Product } from '../data/entities/product.entity';
import { CreateRecipeDTO } from './models/create-recipe.dto';
import { RecipeProduct } from '../data/entities/recipe-product';
import { User as UserEntity } from '../data/entities/user.entity';
import { RecipeCategory } from '../data/entities/recipe-category.entity';
import { RecipeCategoryDTO } from './models/recipe-category.dto';
import { RecipeCategoryList } from '../data/recipe-category-list';
import { ProductsService } from '../products/products.service';
import { NotFoundWithCustomMessageException } from '../common/exceptions/custom-not-found';
import { Nutrition } from '../data/entities/nutrition.entity';
import { NutritionalValueEntryDTO } from './models/nutriotional-value-entry.dto';

@Injectable()
export class RecipesService {
  public constructor(
    @InjectRepository(Recipe) private recipeRepository: Repository<Recipe>,
    @InjectRepository(Product) private productRepository: Repository<Product>,
    @InjectRepository(RecipeProduct)
    private recipeProductRepository: Repository<RecipeProduct>,
    @InjectRepository(RecipeCategory)
    private recipeCategoryRepository: Repository<RecipeCategory>,
    @InjectRepository(Nutrition)
    private nutritionRepository: Repository<Nutrition>,
    private readonly productsService: ProductsService,
  ) {}

  private asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  };

  public async getRecipeById(recipeId: string): Promise<Recipe> {
    const recipe = await this.recipeRepository.findOne({
      where: { id: recipeId },
    });
    if (!recipe || recipe.isDeleted) {
      throw new NotFoundWithCustomMessageException(
        'No recipe found by provided id.',
      );
    }
    return recipe;
  }

  public async getAllRecipes(userId): Promise<ShowRecipeDTO[]> {
    const query = `SELECT * FROM recipes_db.recipes WHERE isDeleted = false AND authorId = '${userId}';`;
    const recipes: Recipe[] = await this.recipeRepository.query(query);

    const recipesToDisplay: ShowRecipeDTO[] = [];
    await this.asyncForEach(recipes, async recipe => {
      const convertedRecipe = plainToClass(ShowRecipeDTO, recipe, {
        excludeExtraneousValues: true,
      });
      recipesToDisplay.push(convertedRecipe);
    });
    return await recipesToDisplay;
  }

  public async getFoodGroupList(): Promise<string[]> {
    const foodGroups: Array<{
      foodGroup: string;
    }> = await this.productRepository.query(
      'SELECT DISTINCT foodGroup FROM recipes_db.products;',
    );
    const foodGroupList: string[] = [];
    await this.asyncForEach(foodGroups, async foodGroup => {
      const foodGroupString = foodGroup.foodGroup;
      foodGroupList.push(foodGroupString);
    });
    return await foodGroupList;
  }

  // todo add return type and any other types necessary
  // public async createRecipe(recipe: CreateRecipeDTO, user: UserEntity): Promise<any> {
  //   const recipeToCreate: Recipe = await this.recipeRepository.create();
  //   recipeToCreate.title = recipe.title;

  //   const recipeToCreateCategory: RecipeCategoryDTO = await this.recipeCategoryRepository.findOne({ where: { category: recipe.foodGroup } });
  //   if (!recipeToCreateCategory) {
  //     return `The category of a recipe must be one of: ${RecipeCategoryList.list.join(', ')}.`;
  //   }
  //   recipeToCreate.foodGroup = recipeToCreateCategory.category;
  //   recipeToCreate.author = Promise.resolve(user);

  //   const partiallyCreatedRecipe = await this.recipeRepository.save(recipeToCreate);
  //   partiallyCreatedRecipe.nutrition = new Nutrition();

  //   // Saves each of the recipe products to the DB
  //   const recipeProductsToSave: RecipeProduct[] = [];
  //   if (recipe.recipeProducts.length > 0) {
  //     await this.asyncForEach(recipe.recipeProducts, (async recipeProduct => {
  //       const recipeProductToSave: RecipeProduct = new RecipeProduct();
  //       recipeProductToSave.product = await this.productsService.getProductByCode(+recipeProduct.productCode);
  //       recipeProductToSave.amount = recipeProduct.amount;
  //       recipeProductToSave.recipes = this.getRecipeById(partiallyCreatedRecipe.id);

  //       const savedRecipeProduct = await this.recipeProductRepository.save(recipeProductToSave);

  //       recipeProductsToSave.push(savedRecipeProduct);
  //     }));
  //   }
  //   // recipeToCreate.recipeProducts = recipeProductsToSave;
  //   partiallyCreatedRecipe.recipeProducts = recipeProductsToSave;

  //   // Subrecipes
  //   // todo check (by ID?) if selected subrecipes aren't deleted
  //   let subrecipes: Recipe[] = [];
  //   if (recipe.childRecipes.length > 0) {
  //     // todo check if recipe.subrecipes[i] isDeleted === false and don't include it if it is
  //     subrecipes = recipe.childRecipes;
  //   }
  //   recipeToCreate.subrecipes = Promise.resolve(subrecipes);
  //   // partiallyCreatedRecipe.childRecipes = subrecipes;
  //   recipeToCreate.nutrition = new Nutrition();
  //   // partiallyCreatedRecipe.nutrition = new Nutrition();

  //   // Nutrition
  //   const recipeNutritionalValues: Nutrition = new Nutrition();
  //   recipeNutritionalValues.product = null;
  //   recipeNutritionalValues.recipe = Promise.resolve(partiallyCreatedRecipe);
  //   // const test = await this.recipeRepository.findOne({ where: { id: partiallyCreatedRecipe.id } });
  //   // recipeNutritionalValues.recipe = Promise.resolve(test);

  //   const protein = recipe.nutritionalValue.find((value) => value.desciption === 'Protein');
  //   recipeNutritionalValues.PROCNT = (Object.assign({}, protein));
  //   const fat = recipe.nutritionalValue.find((value) => value.desciption === 'Total lipid (fat)');
  //   recipeNutritionalValues.FAT = (Object.assign({}, fat));
  //   const carbs = recipe.nutritionalValue.find((value) => value.desciption === 'Carbohydrate, by difference');
  //   recipeNutritionalValues.CHOCDF = (Object.assign({}, carbs));
  //   const energy = recipe.nutritionalValue.find((value) => value.desciption === 'Energy');
  //   recipeNutritionalValues.ENERC_KCAL = (Object.assign({}, energy));
  //   const sugar = recipe.nutritionalValue.find((value) => value.desciption === 'Sugars, total');
  //   recipeNutritionalValues.SUGAR = (Object.assign({}, sugar));
  //   const fiber = recipe.nutritionalValue.find((value) => value.desciption === 'Fiber, total dietary');
  //   recipeNutritionalValues.FIBTG = (Object.assign({}, fiber));
  //   const calcium = recipe.nutritionalValue.find((value) => value.desciption === 'Calcium, Ca');
  //   recipeNutritionalValues.CA = (Object.assign({}, calcium));
  //   const iron = recipe.nutritionalValue.find((value) => value.desciption === 'Iron, Fe');
  //   recipeNutritionalValues.FE = (Object.assign({}, iron));
  //   const phosphorus = recipe.nutritionalValue.find((value) => value.desciption === 'Phosphorus, P');
  //   recipeNutritionalValues.P = (Object.assign({}, phosphorus));
  //   const potassium = recipe.nutritionalValue.find((value) => value.desciption === 'Potassium, K');
  //   recipeNutritionalValues.K = (Object.assign({}, potassium));
  //   const sodium = recipe.nutritionalValue.find((value) => value.desciption === 'Sodium, Na');
  //   recipeNutritionalValues.NA = (Object.assign({}, sodium));
  //   const vitA = recipe.nutritionalValue.find((value) => value.desciption === 'Vitamin A, IU');
  //   recipeNutritionalValues.VITA_IU = (Object.assign({}, vitA));
  //   const vitE = recipe.nutritionalValue.find((value) => value.desciption === 'Vitamin E (alpha-tocopherol)');
  //   recipeNutritionalValues.TOCPHA = (Object.assign({}, vitE));
  //   const vitD = recipe.nutritionalValue.find((value) => value.desciption === 'Vitamin D');
  //   recipeNutritionalValues.VITD = (Object.assign({}, vitD));
  //   const vitC = recipe.nutritionalValue.find((value) => value.desciption === 'Vitamin C, total ascorbic acid');
  //   recipeNutritionalValues.VITC = (Object.assign({}, vitC));
  //   const vitB12 = recipe.nutritionalValue.find((value) => value.desciption === 'Vitamin B-12');
  //   recipeNutritionalValues.VITB12 = (Object.assign({}, vitB12));
  //   const folic = recipe.nutritionalValue.find((value) => value.desciption === 'Folic acid');
  //   recipeNutritionalValues.FOLAC = (Object.assign({}, folic));
  //   const cholesterol = recipe.nutritionalValue.find((value) => value.desciption === 'Cholesterol');
  //   recipeNutritionalValues.CHOLE = (Object.assign({}, cholesterol));
  //   const transFat = recipe.nutritionalValue.find((value) => value.desciption === 'Fatty acids, total trans');
  //   recipeNutritionalValues.FATRN = (Object.assign({}, transFat));
  //   const satFat = recipe.nutritionalValue.find((value) => value.desciption === 'Fatty acids, total saturated');
  //   recipeNutritionalValues.FASAT = (Object.assign({}, satFat));
  //   const monoFat = recipe.nutritionalValue.find((value) => value.desciption === 'Fatty acids, total monounsaturated');
  //   recipeNutritionalValues.FAMS = (Object.assign({}, monoFat));
  //   const polyFat = recipe.nutritionalValue.find((value) => value.desciption === 'Fatty acids, total polyunsaturated');
  //   recipeNutritionalValues.FAPU = (Object.assign({}, polyFat));

  //   const savedRecipeNutritionalValues = await this.nutritionRepository.save(recipeNutritionalValues);

  //   // recipeToCreate.nutrition = await savedRecipeNutritionalValues;
  //   // partiallyCreatedRecipe.nutrition = await savedRecipeNutritionalValues;

  //   // const recipeCategory = await this.recipeCategoryRepository.findOne({ where: { category: recipe.foodGroup } });

  //   // recipeToCreate.recipeCategory = Promise.resolve(recipeCategory);
  //   // partiallyCreatedRecipe.recipeCategory = Promise.resolve(recipeCategory);

  //   return recipeToCreate;

  //   // console.log('after');
  //   // const test = await this.recipeRepository.save(partiallyCreatedRecipe);
  //   // return await test;

  //   //
  //   //

  //   // recipeToCreate.recipeProducts = await Promise.all(recipe.recipeProducts.map(async (item: any) => {
  //   //   const newIngredient: RecipeProduct = this.recipeProdRepository.create();

  //   //   newIngredient.product = item.product;
  //   //   newIngredient.amount = item.amount;

  //   //   const savedIngredient = await this.recipeProdRepository.save(newIngredient);

  //   //   return savedIngredient;
  //   // }));
  //   // // recipeToCreate.recipeProducts[0].amount = await recipe.recipeProducts[0].amount;
  //   // // recipeToCreate.recipeProducts[0].product = await recipe.recipeProducts[0].product;
  //   // // recipeToCreate.recipeProducts[0].product[0].description = await recipe.recipeProducts[0].product[0].description;
  //   // // recipeToCreate.recipeProducts[0].product[0].foodGroup = await recipe.recipeProducts[0].product[0].foodGroup;
  //   // // recipeToCreate.recipeProducts[0].product[0].measures = await recipe.recipeProducts[0].product[0].measures;
  //   // const { title, foodGroup } = recipe;
  //   // const createPostDTO = {
  //   //   title,
  //   //   foodGroup,
  //   // };
  //   // const savedRecipe = await this.recipeRepository.save(recipeToCreate);
  //   // return savedRecipe;
  //   // console.log(recipe.recipeProducts[0].product[0].description);
  //   // console.log(recipe.recipeProducts[0].product[0].foodGroup);
  //   // console.log(recipe.recipeProducts[0].product[0].measures);

  // }

  //
  //

  // todo for delete recipe - check if recipe is used as a subrecipe somewhere (e.g. look for recipe id in the subrecipe column of the table which matches recipe id to subrecipe id)

  // todo for update recipe - overwrite all info for recipe by id
}
