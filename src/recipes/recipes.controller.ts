import {
  Controller,
  UseGuards,
  Get,
  Post,
  Body,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { RecipesService } from './recipes.service';
import { User as UserDecorator } from '../decorators/user.decorator';
import { User as UserEntity } from '../data/entities/user.entity';
import { ShowRecipeDTO } from '../models/show-recipe.dto';
import { CreateRecipeDTO } from './models/create-recipe.dto';
import { BadRequestException } from '../common/exceptions/http-bad-request';

@UseGuards(AuthGuard())
@Controller('recipes')
export class RecipesController {
  public constructor(private readonly recipesService: RecipesService) {}

  @Get()
  public async getAllRecipes(
    @UserDecorator() user: UserEntity,
  ): Promise<ShowRecipeDTO[]> {
    return await this.recipesService.getAllRecipes(user.id);
  }

  // todo remove once create recipe feature is implemented since
  // it will use the getFoodGroupList method from the service
  @Get('fg')
  public async getFoodGroupList(): Promise<string[]> {
    return await this.recipesService.getFoodGroupList();
  }

  // todo add return type and any other types necessary
  // @Post()
  // public async createRecipe(
  //   @UserDecorator() user: UserEntity,
  //   @Body(new ValidationPipe({ whitelist: true, transform: true })) recipe: CreateRecipeDTO,
  // ): Promise<any> {
  //   if (!recipe.title) {
  //     throw new BadRequestException('The recipe title cannot be empty!');
  //   }
  //   if (typeof recipe.title !== 'string') {
  //     throw new BadRequestException('The recipe title must be a string!');
  //   }
  //   if (!recipe.foodGroup) {
  //     throw new BadRequestException('The recipe category cannot be empty!');
  //   }
  //   if (typeof recipe.foodGroup !== 'string') {
  //     throw new BadRequestException('The recipe category must be a string!');
  //   }
  //   return await this.recipesService.createRecipe(recipe, user);
  // }
}
