import { IsString, IsNotEmpty, IsNumber } from 'class-validator';

export class RecipeProductDTO {
  @IsNumber()
  @IsNotEmpty()
  productCode: number;

  @IsNumber()
  @IsNotEmpty()
  amount: number;
}
