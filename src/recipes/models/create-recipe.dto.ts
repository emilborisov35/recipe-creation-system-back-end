import {
  IsString,
  IsNotEmpty,
  IsIn,
  IsArray,
  ValidateNested,
} from 'class-validator';

import { RecipeProductDTO } from './recipe-product.dto';
import { Recipe } from '../../data/entities/recipe.entity';
import { NutritionalValueEntryDTO } from './nutriotional-value-entry.dto';
import { RecipeCategory } from '../../data/entities/recipe-category.entity';

export class CreateRecipeDTO {
  id: string;

  title: string;

  recipeProducts?: [];

  childRecipes?: [];

  nutritionalValue: [];

  recipeCategory: string;
}
