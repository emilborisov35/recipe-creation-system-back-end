import { IsString, IsNotEmpty, IsNumber } from 'class-validator';

export class NutritionalValueEntryDTO {
  @IsString()
  @IsNotEmpty()
  desciption: string;

  @IsString()
  @IsNotEmpty()
  unit: string;

  @IsNumber()
  @IsNotEmpty()
  value: number;
}
