import { IsString, IsNotEmpty, IsIn } from 'class-validator';

export class RecipeCategoryDTO {
  @IsIn(['Soup', 'Appetizer', 'Salads', 'Bread', 'Drink', 'Dessert', 'Main Dish', 'Sauce', 'Side Dish'])
  category: string;
}
