import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { RecipesController } from './recipes.controller';
import { RecipesService } from './recipes.service';
import { AuthModule } from './../auth/auth.module';
import { Recipe } from '../data/entities/recipe.entity';
import { Product } from '../data/entities/product.entity';
import { RecipeProduct } from '../data/entities/recipe-product';
import { RecipeCategory } from '../data/entities/recipe-category.entity';
import { ProductsModule } from '../products/products.module';
import { ProductsService } from '../products/products.service';
import { Nutrition } from '../data/entities/nutrition.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Recipe, Product, RecipeProduct, RecipeCategory, Nutrition]),
    AuthModule,
    ProductsModule,
  ],
  controllers: [RecipesController],
  providers: [RecipesService, ProductsService],
})
export class RecipesModule { }
