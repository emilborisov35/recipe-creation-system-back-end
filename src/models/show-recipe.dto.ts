import { IsString } from 'class-validator';
import { Expose } from 'class-transformer';

import { UserDto } from '../models/users/user.dto';

export class ShowRecipeDTO {
  @Expose()
  id: string;

  @Expose()
  @IsString()
  title: string;

  @Expose()
  @IsString()
  foodGroup: string;

  @Expose()
  author: UserDto;
  
  //author not needed for the purpose of the app imo
}
