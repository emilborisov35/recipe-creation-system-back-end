import { JwtPayload } from '../common/interfaces/jwt-payload';
import { LoginUserDto } from '../models/users/login-user.dto';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../core/services/users.service';
import { User } from '../data/entities/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) { }

  async signIn(userData: LoginUserDto): Promise<string> {
    const { username } = userData;
    const user = await this.usersService.signIn(username);
    if (!user) {
      return null;
    }
    const isPasswordValid = await this.usersService.validatePassword(userData);
    if (!isPasswordValid) {
      return null;
    }
    const userPayload: JwtPayload = { username: user.username };

    return await this.jwtService.sign(userPayload);
  }

  async validateUser(payload: JwtPayload): Promise<User> {
    return await this.usersService.validate(payload);
  }
}
