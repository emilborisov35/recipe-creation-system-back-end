import {
  Controller,
  ValidationPipe,
  Body,
  Post,
  UseGuards,
  UseFilters,
  Delete,
  ForbiddenException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { LoginUserDto } from '../models/users/login-user.dto';
import { AuthService } from './auth.service';
import { BadRequestFilter } from '../common/filters/bad-request.filter';
import { NotFoundFilter } from '../common/filters/not-found.filter';
import { BadRequestException } from '../common/exceptions/http-bad-request';

@UseFilters(BadRequestFilter, NotFoundFilter)
@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) { }

  @Post('session')
  async login(@Body(new ValidationPipe({ transform: true, whitelist: true })) userData: LoginUserDto): Promise<{ token: string }> {
    if (userData.username === '' || userData.password === '') {
      throw new BadRequestException(`The username and password fields cannot be empty.`);
    }
    const token = await this.authService.signIn(userData);

    if (!token) {
      throw new ForbiddenException(`You have entered an incorrect username or password.`);
    }
    return { token };
  }

  @UseGuards(AuthGuard())
  @Delete('session')
  logout(): { message: string } {
    return { message: 'You have successfully logged out.' };
  }
}
