import { IsString, IsNotEmpty, IsIn, IsNumber } from 'class-validator';

import { Product } from '../../data/entities/product.entity';

export class CreateRecipeProductDTO {
  @IsNumber()
  @IsNotEmpty()
  amount: number;

  product: Product;
}
