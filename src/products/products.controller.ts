import {
  Controller,
  UseFilters,
  UseGuards,
  Get,
  Query,
  Post,
  Body,
  Param,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { ProductsService } from './products.service';
import { ProductQueryDto } from '../models/products/product-query.dto';
import { BadRequestException } from '../common/exceptions/http-bad-request';
import { ProductsDto } from '../models/products/products.dto';
import { Product } from '../data/entities/product.entity';
import { NotFoundWithCustomMessageException } from '../common/exceptions/custom-not-found';

@UseGuards(AuthGuard())
@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) { }

  @Get()
  async getProducts(
    @Query() query: ProductQueryDto,
  ): Promise<ProductsDto | string> {
    const route: string = `localhost:3000/products`;

    if (
      JSON.stringify(query) === `{}` ||
      (!JSON.stringify(query).includes('description') &&
        !JSON.stringify(query).includes('foodGroup')) ||
      (!query.description && !query.foodGroup) ||
      (!isNaN(+query.description) && !isNaN(+query.foodGroup))
    ) {
      throw new BadRequestException(
        `Please specify a product that you would like to search for!`,
      );
    }
    return this.productsService.getProducts(query, route);
  }

  @Get(':productCode')
  public async getProductByCode(
    @Param() productCode: number,
  ): Promise<Product> {
    const product = await this.productsService.getProductByCode(+productCode);
    if (!product) {
      throw new NotFoundWithCustomMessageException(
        'No product found for the provided product code.',
      );
    }
    return product;
  }

  // @Post()
  // async createRecipeProduct(@Body() recipeProduct: ProductQueryDto) {
  //   const route: string = `localhost:3000/products`;

  //   return this.productsService.getProducts(query, route);
  // }
}
