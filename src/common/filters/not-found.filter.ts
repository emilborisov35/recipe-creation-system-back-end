import { ExceptionFilter, Catch, ArgumentsHost, HttpStatus } from '@nestjs/common';
import { Request, Response } from 'express';
import { NotFoundException } from '../exceptions/not-found';

@Catch(NotFoundException)
export class NotFoundFilter implements ExceptionFilter {
  catch(exception: NotFoundException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = HttpStatus.NOT_FOUND;
    const timestamp = new Date();

    const errorResponse = {
      statusCode: status,
      date: timestamp.toLocaleDateString(),
      time: timestamp.toLocaleTimeString(),
      path: request.url,
      method: request.method,
      message: exception.message,
    };

    response
      .status(status)
      .json(errorResponse);
  }
}
