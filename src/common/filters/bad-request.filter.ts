import { BadRequestException } from './../exceptions/bad-request';
import { ExceptionFilter, Catch, ArgumentsHost, HttpStatus } from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(BadRequestException)
export class BadRequestFilter implements ExceptionFilter {
  catch(exception: BadRequestException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = HttpStatus.FORBIDDEN;
    const timestamp = new Date();

    const errorResponse = {
      statusCode: status,
      date: timestamp.toLocaleDateString(),
      time: timestamp.toLocaleTimeString(),
      path: request.url,
      method: request.method,
      message: exception.message,
    };

    response
      .status(status)
      .json(errorResponse);
  }
}
