import { HttpException, HttpStatus } from '@nestjs/common';

export class BadRequestException extends HttpException {
  constructor(reason: string) {
    super(reason, HttpStatus.BAD_REQUEST);
  }
}
