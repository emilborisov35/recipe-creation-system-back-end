export class NotFoundWithCustomMessageException extends Error {
  constructor(reason: string) {
    super(reason);
  }
}
