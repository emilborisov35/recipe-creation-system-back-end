import { Module } from '@nestjs/common';
import { RecipesInMemoryService } from './recipes-in-memory.service';
import { RecipesInMemoryController } from './recipes-in-memory.controller';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  providers: [RecipesInMemoryService],
  controllers: [RecipesInMemoryController],
})
export class RecipesInMemoryModule {}
