import {
  Controller,
  Post,
  HttpCode,
  HttpStatus,
  Body,
  BadRequestException,
  Get,
  Query,
  Param,
  NotFoundException,
  Put,
  Delete,
} from '@nestjs/common';
import { RecipesInMemoryService } from './recipes-in-memory.service';
import { CreateRecipeDTO } from '../recipes/models/create-recipe.dto';

@Controller('recipes-in-memory')
export class RecipesInMemoryController {
  public constructor(
    private readonly recipesDataService: RecipesInMemoryService,
  ) { }

  @Get()
  @HttpCode(HttpStatus.OK)
  public getAllRecipes(@Query('title') title?: string) {
    if (title) {
      return this.recipesDataService
        .getAllRecipes()
        .filter(recipe =>
          recipe.title.toLowerCase().includes(title.toLowerCase()),
        );
    }
    return this.recipesDataService.getAllRecipes();
  }

  @Get('/:id')
  @HttpCode(HttpStatus.OK)
  public getRecipe(@Param('id') id: string) {
    const foundRecipe = this.recipesDataService.getRecipeById(id);
    if (!foundRecipe) {
      throw new NotFoundException('Recipe not found!');
    }

    return foundRecipe;
  }

  @Get('/title/:title')
  @HttpCode(HttpStatus.OK)
  public getRecipeByTitle(@Param('title') title: string) {
    const foundRecipe = this.recipesDataService.getRecipeByTitle(title);
    if (!foundRecipe) {
      throw new NotFoundException('Recipe not found!');
    }

    return foundRecipe;
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  public addRecipe(@Body() recipe: CreateRecipeDTO) {
    const {
      id,

      title,

      recipeProducts,

      childRecipes,

      nutritionalValue,

      recipeCategory,
    } = recipe;
    const addRecipeDTO: CreateRecipeDTO = {
      id,

      title,

      recipeProducts,

      childRecipes,

      nutritionalValue,

      recipeCategory,
    };

    // if (Object.values(addRecipeDTO).some(prop => !prop)) {
    //   // validate the properties
    //   throw new BadRequestException('Invalid recipe properties!');
    // }

    return this.recipesDataService.addRecipe(addRecipeDTO);
  }
  @Put('/:id')
  @HttpCode(HttpStatus.OK)
  public updateRecipe(@Param('id') recipeId: string, @Body() updatedRecipe) {
    const {
      id,
      title,
      recipeProducts,
      childRecipes,
      nutritionalValue,
      recipeCategory,
    } = updatedRecipe;
    const updateRecipeDTO: CreateRecipeDTO = {
      id,

      title,

      recipeProducts,

      childRecipes,

      nutritionalValue,

      recipeCategory,
    };

    // if (Object.values(updateRecipeDTO).some(prop => !prop)) {
    //   // validate the properties
    //   throw new BadRequestException('Invalid recipe properties!');
    // }

    const foundRecipe = this.recipesDataService.getRecipeById(recipeId);
    if (!foundRecipe) {
      throw new NotFoundException('Recipe not found!');
    }

    return this.recipesDataService.updateRecipe2(
      foundRecipe.id,
      updateRecipeDTO,
    );
  }
  @Delete('/:id')
  @HttpCode(HttpStatus.OK)
  public deleteRecipe(@Param('id') id: string) {

    const foundRecipe = this.recipesDataService.getRecipeById(id);
    if (!foundRecipe) {
      throw new NotFoundException('Recipe not found!');
    }

    return this.recipesDataService.deleteRecipe(foundRecipe.id);
  }
}
