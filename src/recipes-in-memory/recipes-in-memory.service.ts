import { Injectable } from '@nestjs/common';
import { Database } from '../database/abstract/database';
import { CreateRecipeDTO } from '../recipes/models/create-recipe.dto';
import { Recipe } from '../data/entities/recipe.entity';

// generator function
// const generateId1 = (function*() {
//   let id = 0;

//   while (true) {
//     yield id++;
//   }
// })();
const generateId = (function* () {
  // Math.random should be unique because of its seeding algorithm.
  // Convert it to base 36 (numbers + letters), and grab the first 9 characters
  // after the decimal.
  return (
    '_' +
    Math.random()
      .toString(36)
      .substr(2, 9)
  );
})();

@Injectable()
export class RecipesInMemoryService {
  public constructor(private readonly database: Database<CreateRecipeDTO>) { }

  public getAllRecipes() {
    return this.database.all();
  }

  public getRecipeById(recipeId: string) {
    return this.database.find(recipeId);
  }

  public getRecipeByTitle(recipeTitle: string) {
    return this.database.findByName(recipeTitle);
  }

  public addRecipe(recipe: CreateRecipeDTO) {
    const id: string = (Math.random()
      * 100000)
      .toString();
    const recipeEntity = { ...recipe, id };

    return this.database.add(recipeEntity);
  }

  public updateRecipe(oldRecipeId: string, newRecipe: CreateRecipeDTO) {
    const oldRecipeEntity = this.database.find(oldRecipeId);
    const newRecipeEntity = { ...oldRecipeEntity, ...newRecipe };

    return this.database.update(newRecipeEntity);
  }

  public updateRecipe2(id2: string, newRecipe: CreateRecipeDTO) {
    this.deleteRecipe(id2);
    const id: string = id2;
    const recipeEntity = { ...newRecipe, id };

    return this.database.add(recipeEntity);
  }

  public deleteRecipe(RecipeId: string) {
    const RecipeEntity = this.database.find(RecipeId);

    return this.database.delete(RecipeEntity);
  }
}
