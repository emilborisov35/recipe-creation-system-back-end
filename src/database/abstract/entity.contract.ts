export interface Entity {
  id: string;
  title: string;
}
