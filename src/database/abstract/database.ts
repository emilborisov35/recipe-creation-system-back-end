import { Entity } from './entity.contract';

export abstract class Database<T extends Entity> {
  public abstract all(): T[];

  public abstract find(entityId: string): T;

  public abstract findByName(entityName: string): T;

  public abstract add(entity: T): T;

  public abstract update(entity: T): T;

  public abstract delete(entity: T): T;
}
