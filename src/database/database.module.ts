import { Module } from '@nestjs/common';
import { Database } from './abstract/database';
import { FileStorageDatabase } from './file-storage/file-storage-database';
import { InMemoryDatabase } from './in-memory-storage/in-memory-database';

@Module({
  // change here to use the InMemoryDatabase
  providers: [{ provide: Database, useClass: FileStorageDatabase }],
  exports: [Database],
})
export class DatabaseModule {}
