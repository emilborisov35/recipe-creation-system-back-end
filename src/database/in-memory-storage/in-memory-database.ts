import { Entity } from '../abstract/entity.contract';
import { Database } from '../abstract/database';

export class InMemoryDatabase<T extends Entity> extends Database<T> {
  // tslint:disable-next-line: variable-name
  private readonly _data: T[] = [];

  public all(): T[] {
    return this._data.slice();
  }

  public find(entityId: string): T {
    return this._data.find(x => x.id === entityId);
  }
  public findByName(entityName: string): T {
    return this._data.find(x => x.title === entityName);
  }

  public add(entity: T): T {
    this._data.push(entity);

    return entity;
  }

  public update(entity: T): T {
    const index: number = this._data.findIndex(x => x.id === entity.id);
    this._data[index] = entity;

    return entity;
  }

  public delete(entity: T): T {
    const index: number = this._data.findIndex(x => x.id === entity.id);
    this._data.splice(index, 1);

    return entity;
  }
}
